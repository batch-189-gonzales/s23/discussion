/*
	OBJECTS - data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

	Syntax:
		let objectName = {
			key1: value1,
			key2: value2,
			...
			keyN: valueN
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects");
console.log(cellphone);
console.log(typeof cellphone);

//Creating objects using a constructor function
/*
	Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

	Syntax:
		function ObjectName(value1, value2, ..., valueN) {
			this.key1 = value1,
			this.key2 = value2,
			...
			this.keyN = valueN
		};
*/

function Laptop (name, manufactureDate) {
	this.name = name,
	this.manufactureDate = manufactureDate
};

let lenovo = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using construtor function");
console.log(lenovo);

let msi = new Laptop("MSI", 2021);
console.log("Result of creating objects using construtor function");
console.log(msi);

let macbook = Laptop("Macbook", 2022);
console.log("Result of creating objects using construtor function without new keyword");
console.log(macbook);

//Creating empty objects
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

//Accessing Object Property using the dot notation
console.log(`Result from dot notation: ${msi.name}`);

//Accessing Object Property using the square bracket notation
console.log(`Result from square bracket notation: ${msi["name"]}`);

//Accessing array objects
let array = [lenovo, msi];
console.log(array[0]["name"]);
console.log(array[0].name);

//Initializing/Adding/Deleting/Reassigning Object Properties
let car = {};
console.log(car);

//Add
car.name = "Honda Civic";
console.log(`Result from adding property using dot notation:`);
console.log(car);

car["manufacture date"] = 2019;
console.log(`Result from adding property using square bracket notation:`);
console.log(car);

//Delete
delete car["manufacture date"];
console.log(`Result from deleting property using square bracket notation:`);
console.log(car);

//Reassign
car.name = "Tesla";
console.log(`Result from reassigning property using dot notation:`);
console.log(car);

//Object Methods
/*
	A method is a function which is a property of an object. They are also functions and one of the key differences they have is that methods are functions related to a specific object,
*/

let person = {
	name: "John",
	talk: function () {
		console.log(`Hello! My name is ${this.name}`);
	}
};

console.log(person);
console.log(`Result from object methods:`);
person.talk();

person.walk = function () {
	console.log(`${this.name} walked 25 steps forward.`);
};

console.log(`Result from object methods:`);
person.walk();

let friend = {
	firstName: "Nehemiah",
	lastName: "Ellorico",
	address: {
		city: "Austin, Texas",
		country: "USA"
	},
	emails: ["nejellorico@mail.com", "nej123@mail.com"],
	introduce: function () {
		console.log(`Hello! My name is ${this.firstName} ${this.lastName}.`);
	}
};

friend.introduce();